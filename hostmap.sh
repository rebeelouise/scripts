#!/bin/bash/
#use to map single read fastq to host transcriptomes or genomes
#script to be used in the directory with uniquely named fastq files within it
#script makes directories above the file with fastq's in for the host alignments 
#script also stores host removed bams and fastq files which may be of use for metatranscriptomic analysis

mkdir -p ../host_alignments/sam
mkdir -p ../host_alignments/bam
mkdir -p ../host_alignments/sorted_bam
mkdir -p ../host_removed/bam
mkdir -p ../host_removed/fastq

ref=$1
threads=$2

for i in $(ls *.fastq | sed 's/.fastq//' | uniq)
do
	minimap2 -ax sr -t ${threads} ${ref} ${i}.fastq > ../host_alignments/sam/${i}.sam
	samtools view -bS ../host_alignments/sam/${i}.sam > ../host_alignments/bam/${i}.bam
	samtools view -b -f 4 ../host_alignments/bam/${i}.bam > ../host_removed/bam/${i}.bam
	samtools bam2fq ../host_removed/bam/${i}.bam > ../host_removed/fastq/{i}.fastq
	samtools sort -@ ${threads} ../host_alignments/bam/${i}.bam > ../host_alignments/sorted_bam/sorted_${i}.bam
	samtools index ../host_alignments/sorted_bam/sorted_${i}.bam
done
