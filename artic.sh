#!/bin/bash/
mkdir -p barplot
mkdir -p consensus
primers=$1
for i in $(ls *.fastq | sed 's/.fastq//' | uniq)
	do 
mkdir -p ${i}_artic
		artic minion --medaka --normalise 200 --threads 60 --scheme-directory /home/hlrpenri/tools/artic-ncov2019/primer_schemes --read-file ${i}.fastq  nCoV-2019/$primers ${i}	
cp *barplot.png barplot/
cp *.consensus.fasta consensus/
mv *${i}.* ${i}_artic/
mv *${i}-* ${i}_artic/
	done 


