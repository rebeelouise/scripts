#!/bin/bash/
min=$1
max=$2

mkdir -p filtered


for i in $(ls *.fastq | sed 's/.fastq//' | uniq)

	do
		echo $i
	       
		NanoFilt ${i}.fastq -l $1 --maxlength $2 > filtered/filtered_${i}.fastq 
	done
