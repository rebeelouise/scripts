#attempt at automating workflow
#!/bin/bash/
#usage sh alignment.sh fastq.fastq genome.fa
#initialuise a variable with an intuitive name to store the name of the input fastq file

fastq=$1

# grab base of filename for naming outputs

base=`basename $fastq .subset.fastq`
echo "Sample name is $base"

# specify the number of cores to use

cores=12

# directory with genome reference FASTA
genome=$2

# make all of the output directories
# The -p option means mkdir will create the whole path if it
# does not exist and refrain from complaining if it does exist

mkdir -p /home/hlrpenri/results/fastqc/$base
mkdir -p /home/hlrpenri/results/alignments/$base

# set up output filenames and locations

fastqc_out=/home/hlrpenri/results/fastqc/$base/
align_out=/home/hlrpenri/results/alignments/$base/${base}
sam_out=/home/hlrpenri/results/alignments/$base/sorted_${base}.sam
markdup_out=/home/hlrpenri/results/alignments/$base/markdup_${base}.sam
metrics=/home/hlrpenri/results/alignments/$base/metrics_${base}.txt

#do the thing

echo "Processing file $fastq"
set -x

# Run FastQC and move output to the appropriate folder
fastqc $fastq_out $fastq

#minimap

minimap2 -t $cores -ax map-ont $genome $fastq > ${align_out}.sam
samtools sort -@ $cores ${align_out}.sam > ${align_out}_sorted.sam
picard MarkDuplicates I=${align_out}_sorted.sam O=${align_out}_markdup.sam M=$metrics

echo "counting average depth"
samtools depth -a ${align_out}_markdup.sam | awk '{c++;s+=$3}END{print s/c}'
echo "counting coverage"
samtools depth -a  ${align_out}_markdup.sam | awk '{c++; if($3>0) total+=1}END{print (total/c)*100}'
echo "counting coverage 20X"
samtools depth -a  ${align_out}_markdup.sam | awk '{c++; if($3>20) total+=1}END{print (total/c)*100}'


