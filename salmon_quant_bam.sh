#!/bin/bash/
#script to quant host transcripts using salmon based on single read data
#to be ran in the directory with the alignment files in sorted bam format
#usage sh salmon_quant_bam.sh number_of_threads path/to/transcriptome/ path/to/output

threads=$1
transcriptome=$2
quants=$3

mkdir -p ${quants}

for i in $(ls *.bam | sed 's/.bam//' | uniq)
do 
	salmon quant --noErrorModel -p ${threads} -t ${transcriptome} -l U -a ${i}.bam -o ${quants}/${i}/
done
