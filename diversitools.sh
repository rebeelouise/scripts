#!/bin/bash/
#take alignment file to virus to determine the amino acid change derived from SNPs

for i in $(ls *.bam | sed 's/.bam//' | uniq)
do
	mkdir -p ../DiversiTools
perl /home/hlrpenri/tools/DiversiTools/bin/diversiutils.pl -bam ${i}.bam -ref /home/hlrpenri/tools/artic-ncov2019/primer_schemes/nCoV-2019/V3/nCoV-2019.reference.fasta -orfs /home/hlrpenri/tools/DiversiTools/bin/CodingRegion2.txt -stub ../DiversiTools/${i}
done
