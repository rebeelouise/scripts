#!/bin/bash/
# usage sh basecall.sh /path/to/fast5s /path/to/output/directory barcode kit
#variable 1 refers to the path/to/fast5
input=$1

#variable 2 defines the output directory for the fastq files
output=$2

#variable 3 requires the barcode kits, for example EXP-NBD104 EXP-NBD114 or EXP-NBD196
#barcode=$3

#make directory for the output files, the use of the -p parameter means that if it already exists an error will not be thrown
mkdir -p $output
 
#guppy basecall using appropriate parameters

guppy_basecaller -i $input -s $output -c dna_r9.4.1_450bps_hac.cfg –-qscore_filtering –-barcode_kits "EXP-NBD104 EXP-NBD114" –-device auto --require_barcodes_both_ends

