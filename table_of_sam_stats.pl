#!/usr/bin/perl
# This takes a sam file and tells you a few things about the transcripts (longest one etc)
#
# table_of_sam_stats.pl input.sam
#
#

@files = @ARGV;
$mapq = $ARGV[1];

$window = 20;
open(INFILE, "$ARGV[0]"); # opens samfile

$start_time = time;


%for_translation = (TTT=>"F", TTC=>"F", TCT=>"S", TCC=>"S", TAT=>"Y", TAC=>"Y", TGT=>"C", TGC=>"C", TTA=>"L", TCA=>"S", TAA=>"*", TGA=>"*", TTG=>"L", TCG=>"S", TAG=>"*", TGG=>"W", CTT=>"L", CTC=>"L", CCT=>"P", CCC=>"P", CAT=>"H", CAC=>"H", CGT=>"R", CGC=>"R", CTA=>"L", CTG=>"L", CCA=>"P", CCG=>"P", CAA=>"Q", CAG=>"Q", CGA=>"R", CGG=>"R", ATT=>"I", ATC=>"I", ACT=>"T", ACC=>"T", AAT=>"N", AAC=>"N", AGT=>"S", AGC=>"S", ATA=>"I", ACA=>"T", AAA=>"K", AGA=>"R", ATG=>"M", ACG=>"T", AAG=>"K", AGG=>"R", GTT=>"V", GTC=>"V", GCT=>"A", GCC=>"A", GAT=>"D", GAC=>"D", GGT=>"G", GGC=>"G", GTA=>"V", GTG=>"V", GCA=>"A", GCG=>"A", GAA=>"E", GAG=>"E", GGA=>"G", GGG=>"G");
%rev_translation = (GGC=>"A", ACT=>"S", TCA=>"*", ACA=>"C", TCG=>"R", GAT=>"I", GTT=>"N", GCT=>"S", GTA=>"Y", TGT=>"T", CGA=>"S", CGG=>"P", CAG=>"L", TGC=>"A", CAC=>"V", CTT=>"K", AAC=>"V", GTG=>"H", TCT=>"R", GGT=>"T", TGG=>"P", CCA=>"W", GAG=>"L", GCG=>"R", CAA=>"L", TTA=>"*", CTG=>"Q", CGT=>"T", CAT=>"M", TTT=>"K", TAC=>"V", CTA=>"*", AAG=>"L", TCC=>"G", GAC=>"V", GCA=>"C", TGA=>"S", AAT=>"I", ATA=>"Y", ATT=>"N", AGT=>"T", TTG=>"Q", GTC=>"D", ACC=>"G", GGA=>"S", AAA=>"F", CCT=>"R", ACG=>"R", CCG=>"R", ATG=>"H", TAT=>"I", GGG=>"P", CCC=>"G", TAA=>"L", CTC=>"E", TAG=>"L", ATC=>"D", AGA=>"S", GAA=>"F", CGC=>"A", GCC=>"G", AGC=>"A", TTC=>"E", AGG=>"P");
%base_pair = (G=>"A", A=>"T", T=>"A", C=>"G");
print "hi\n";
$shortest_transcript = 100000000;
%contig_longest = ();
%contig_shortest = ();
%bin = ();
%contig_bin = ();
%sam_file = ();
%uniq_transcripts = ();
%dels = ();
%ins = ();
%depth = ();
%indel_tab = ();


$longest_transcript = 0;
$longest_transcript_name = "";
$transcript_count = 0;

#Start going through the sam file to find the starts and ends of positive and negative mapped transcripts
while($line = <INFILE>)
{
    chomp $line;
    if (substr($line,0,1) eq "@") {$sam_head = $sam_head."$line\n"; next;}
    @samcells = split(/\t/, $line);
    $contig = $samcells[2];
    #if ($contig eq "*") {next;}
    $transcript_name = $samcells[0];
    $uniq_transcripts{$transcript_name} ++;
    if (exists $contig_shortest{$temp_contig}) {
    # do nothing
    } else {$contig_shortest{$temp_contig} = 100000000;}
    $flag = $samcells[1];
    if ($flag & 2048) {$sam_file_sup{$transcript_name} = $line; print TEMP "$line\n"; next;}
    $transcript_count ++;
    #print "$transcript_count\n";
    #if (($transcript_count % 10000) == 0) {print "$transcript_count entries processed\n";}
    $position = $samcells[3];
    $sequence = uc $sam_cells[9];
    $sequence =~ tr/Uu/TT/;
    $sam_mapq = $samcells[4];
    if ($sam_mapq < $mapq) {next;}
    $sam_transcript = $samcells[9];

    @cigar = split(/(M|I|D|N|S|H|P|X|=)/, $samcells[5]);
    $sam_position  = 0;
    $array_cigar = 1;
    #print "$samcells[5]\n$cigar[1]\n";
    
    while (length($cigar[$array_cigar]) >=1){
        # THIS IS A MESS!!!
        $cigar_value = $cigar[$array_cigar - 1];
        if ($cigar[$array_cigar] =~ /[SH]/){
            #print "hello SH\n";
            #$gff_start = $gff_start + $cigar[$array_cigar - 1];
        }
        if (($cigar[$array_cigar] =~ /[S]/) and (($array_cigar + 1) < scalar (@cigar)))
             {
                
                 $sam_position = $sam_position + $cigar_value;
             }
        if ($cigar[$array_cigar] =~ /[M]/){
            
            $temp_count = 1;
            #print "M";
            while ($temp_count <= $cigar_value)
            {
                $depth{$position} ++;
                $sam_position ++;
                $position ++;
                $temp_count ++;
                
            }
            
            #print "hello M\n";
            #$position = $position + $cigar[$array_cigar - 1];
        }
        if ($cigar[$array_cigar] =~ /[D]/){
            
            $temp_cv = $cigar_value - 1;
            $tmp_name = "$chromosome\tDeletion\t$position\t$cigar_value\t ";
            if (exists $indel_tab{$tmp_name}){$indel_tab{$tmp_name} ++;} else {$indel_tab{$tmp_name} = 1;}
            
            $tmp_loc = $contig."\t".$position."\t".$cigar[$array_cigar - 1];
            $dels{$tmp_loc} ++;
            
            $position = $position + $cigar[$array_cigar - 1];
            
            
        }
        if ($cigar[$array_cigar] =~ /[N]/){
            $position = $position + $cigar[$array_cigar - 1];
        }
        if ($cigar[$array_cigar] =~ /[I]/){
            
            $insertion = substr ($sequence, $sam_position, $cigar_value);
            $tmp_name = "$chromosome\tInsertion\t$position\t$cigar_value\t$insertion";
            if (exists $indel_tab{$tmp_name}){$indel_tab{$tmp_name} ++;} else {$indel_tab{$tmp_name} = 1;}
            $tmp_loc = $contig."\t".$position."\t".$cigar[$array_cigar - 1];
            $ins{$tmp_loc} ++;
            $sam_position = $sam_position + $cigar_value;
        }
        
        $array_cigar = $array_cigar + 2;
        
    }
    
    
    
    if ($flag & 16){
        $strand = "-";
        $pola_loc = $contig."\t".$strand."\t".$samcells[3]."\t".$position;
        $polya_pos_neg {$pola_loc} ++;
        if (exists $polya_pos_neg_names {$pola_loc}) {$polya_pos_neg_names {$pola_loc} = $polya_pos_neg_names {$pola_loc}.$transcript_name."\t";} else {$polya_pos_neg_names {$pola_loc} = $transcript_name."\t";}
        $five_loc = $contig."\t".$strand."\t".$position;
        
        $sam_file {$transcript_name} = $line;
        $mod_sam_file {$transcript_name} = $position."\t".$line;
        
        $both_loc = $polya_loc."\t".$position;
        $start_pos_neg {$five_loc} ++;
        if (exists $start_pos_neg_names {$five_loc}) {$start_pos_neg_names {$five_loc} = $start_pos_neg_names {$five_loc}.$transcript_name."\t";} else {$start_pos_neg_names {$five_loc} = $transcript_name."\t";}
        #$position = $samcells[3];
        
        
    } else {
        $strand = "+";
        $pola_loc = $contig."\t".$strand."\t".$position."\t".$samcells[3];
       
        $polya_pos_pos {$pola_loc} ++;
        #print "$pola_loc\t$polya_pos_pos{$pola_loc}\n";
        if (exists $polya_pos_pos_names {$pola_loc}) {$polya_pos_pos_names {$pola_loc} = $polya_pos_pos_names {$pola_loc}.$transcript_name."\t";} else {$polya_pos_pos_names {$pola_loc} = $transcript_name."\t";}
        
        $five_loc = $contig."\t".$strand."\t".$samcells[3];
       
        $sam_file {$transcript_name} = $line;
        $mod_sam_file {$transcript_name} = $position."\t".$line;
        
        $both_loc = $five_loc."\t".$position;
        $start_pos_pos {$five_loc} ++;
        #print TEMP "$transcript_name\t$samcells[3]\t$position\n";
        if (exists $start_pos_pos_names {$five_loc}) {$start_pos_pos_names {$five_loc} = $start_pos_pos_names {$five_loc}.$transcript_name."\t";} else {$start_pos_pos_names {$five_loc} = $transcript_name."\t";}
        
       
    }
    $tran_length = length ($sam_transcript);
    $trunc_len = int (length ($sam_transcript)/100);
    $temp_contig = $contig."\t".$trunc_len;
    $bin{$trunc_len} ++;
    $contig_bin{$temp_contig} ++;
   
    if ($tran_length < $contig_shortest{$temp_contig}) {$contig_shortest{$temp_contig} = $tran_length;}
    if ($tran_length > $contig_longest{$temp_contig}) {$contig_longest{$temp_contig} = $tran_length;}
    
    if ($tran_length > $longest_transcript) {
        $longest_transcript = length ($sam_transcript);
        $longest_transcript_name = "longest transcript is mapped to chromosome $contig strand $strand at location $samcells[3] to $position and is $longest_transcript nucleotides long\n";
        #print "$longest_transcript $contig $position\n";
    }
    
    
    
}


$uniq_transcript = scalar keys %uniq_transcripts;
print $longest_transcript_name;
print "Count of sam entries is $transcript_count and there are $uniq_transcript distinct transcripts mapped\n";


open (TEMP, ">All_lengths_table.txt");
print TEMP $longest_transcript_name."Length - 100nt window\tNumber of transcripts\n";
foreach $value (sort {$bin{$a} <=> $bin{$b} } keys %bin){
    print TEMP $value."00\t$bin{$value}\n";
}
close TEMP;

open (TEMPB, ">All_lengths_contig_table.txt");
print TEMP $longest_transcript_name."Length - 100nt window\tNumber of transcripts\n";
foreach $value (sort {$contig_bin{$a} <=> $contig_bin{$b} } keys %contig_bin){
    print TEMPB $value."\t$bin{$value}\n";
}
close TEMPB;


open (TEMPC, ">All_deletions_table.txt");
print TEMPC "Contig\tLocation\tlength\tCount\n";
foreach $value (sort {$dels{$a} <=> $dels{$b} } keys %dels){
    print TEMPC "$value\t$dels{$value}\n";
}
close TEMPC;

open (TEMPD, ">All_insertions_table.txt");
print TEMPD "Contig\tLocation\tlength\tCount\n";
foreach $value (sort {$ins{$a} <=> $ins{$b} } keys %ins){
    print TEMPD "$value\t$ins{$value}\n";
}
close TEMPD;

$time_elapsed = time - $start_time;
print "Time elapsed is $time_elapsed seconds\n";

open (OUTINDELKEYSIG, ">$ARGV[0].Significant_key_indels.txt");
open (OUTINDELKEY, ">$ARGV[0].key_indels.txt");
open (CTSO, ">$ARGV[0].CTSO.txt");
$warning = "";
# $tmp_name = "$chromosome\tDeletion\t$genome_position\t$temp_cv\t$insertion"; temp_cv is the size of the indel
print OUTINDELKEY "Chromosome\tInsertion or deletion\tLocation\tIndel size\tInsertion seq\tObservation count\tDepth at this lcation\tProportion of observation vs depth\n";
print OUTINDELKEYSIG "Chromosome\tInsertion or deletion\tLocation\tIndel size\tInsertion seq\tObservation count\tDepth at this lcation\tProportion of observation vs depth\n";
foreach $key (keys %indel_tab){
    $value = $indel_tab{$key};
    @array = split(/\t/, $key);
    $genome_position = $array[2]; $indel_len = $array[3];
    if ($depth{$genome_position} > 0) {$a = ($value / $depth{$genome_position}); $tmp_proportion  = sprintf ("%.2f",$a);} else {$tmp_proportion = "zero depth here";}
    
    print OUTINDELKEY "$key\t$value\t$depth{$genome_position}\t$tmp_proportion\n";
    if ($tmp_proportion > 0.1) {
        
        if ($indel_len > 6) {print "\n\nCtSo\nInsertion or deletion\t$array[1]\nLocation\t$genome_position\nIndel size\t$indel_len\nObservation count\t$value\nDepth at this location\t$depth{$genome_position}\nProportion of observation vs depth\t$tmp_proportion\n\n";
            print CTSO "\n\nCtSo\nInsertion or deletion\t$array[1]\nLocation\t$genome_position\nIndel size\t$indel_len\nObservation count\t$value\nDepth at this location\t$depth{$genome_position}\nProportion of observation vs depth\t$tmp_proportion\n\n";
            $warning = $warning."$key\t$value\t$depth{$genome_position}\t$tmp_proportion\n";}
        print OUTINDELKEYSIG "$key\t$value\t$depth{$genome_position}\t$tmp_proportion\n";
    }
}
if ($warning ne "") {print OUTINDELKEYSIG "\n\nCtSo\n\n$warning\n";}
exit;



