#!/bin/bash/


for i in $(ls *.fastq | sed 's/.fastq//' | uniq)

	do
		echo $i
	       
		NanoFilt ${i}.fastq --headcrop 18 --tailcrop 18 > ../trimmed_fastq/trimmed_${i}.fastq 
	done
